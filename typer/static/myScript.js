$(function () {
  $("#visibility-filter a").on("click", function () {
    var value = $(this).attr("data-filter");

    $("table.groups tr").show();

    switch (value) {
      case "member":
        $("table.groups tbody tr").hide();
        $("table.groups tbody tr.member").show();
        break;

      case "no-member":
        $("table.groups tbody tr").hide();
        $("table.groups tbody tr:not(.member)").show();
        break;

      default:
        $("table.groups tbody tr").show();
    }
  });
});

$(function() {

  $("table.tournament tbody tr").hide();
  $("table.tournament tbody tr.to-type").show();
  $("table.tournament thead th.4th-col").show();
  document.getElementById("table-text").innerHTML = "Mecze do wytypowania";
  
  $("#visibility-filter-tournament a").on("click", function () {
    var value = $(this).attr("data-filter");

    switch (value) {

      case "waiting-for-start":
        $('#4-th-col').html('Rozpoczęcie');
        $("table.tournament tbody tr").hide();
        $("table.tournament tbody tr:not(.to-type)").show();
        $("table.tournament thead th.4th-col").hide();
        document.getElementById("table-text").innerHTML = "Mecze oczekujące na rozpoczęcie";
        break;
      
      case "ongoing":
        $('#4-th-col').html('Zakończenie');
        $("table.tournament tbody tr").hide();
        $("table.tournament tbody tr.ongoing").show();
        $("table.tournament thead th.4th-col").hide();
        document.getElementById("table-text").innerHTML = "Trwające mecze";
        break;

      case "finished":
        $('#4-th-col').html('Zakończono');
        $("table.tournament tbody tr").hide();
        $("table.tournament tbody tr.finished").show();
        $("table.tournament thead th.4th-col").hide();
        document.getElementById("table-text").innerHTML = "Zakończone mecze";
        break;

      default:
        $('#4-th-col').html('Rozpoczęcie');
        $("table.tournament tbody tr").hide();
        $("table.tournament tbody tr.to-type").show();
        $("table.tournament thead th.4th-col").show();
        document.getElementById("table-text").innerHTML = "Mecze do wytypowania";
        break;
    }
    });
  });