#!/usr/bin/python
# coding: utf-8
from django import forms
from django.contrib.auth.models import User
from .models import Group, BetsTournament, League, Team, Match, Bet
from django.utils import timezone
from datetime import datetime


class NewGroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['name', 'private', 'password']
    
    def clean_name(self):
        name = self.cleaned_data['name']
        if Group.objects.filter(name=name).exists():
            raise forms.ValidationError('Grupa o podanej nazwie już istnieje.')
        return name


class EditGroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['name', 'private', 'password']
    
    def clean_name(self):
        name = self.cleaned_data['name']
        if Group.objects.filter(name=name).exists():
            raise forms.ValidationError('Grupa o podanej nazwie już istnieje.')
        return name


class NewBetsTournamentForm(forms.ModelForm):
    class Meta:
        model = BetsTournament
        fields = ['name']


class EditBetsTournamentForm(forms.ModelForm):
    class Meta:
        model = BetsTournament
        fields = ['name']


class NewLeagueForm(forms.ModelForm):
    class Meta:
        model = League
        fields = ['name']


class EditLeagueForm(forms.ModelForm):
    class Meta:
        model = League
        fields = ['name']


class NewTeamForm(forms.ModelForm):
    name = forms.CharField()
    
    class Meta:
        model = Team
        fields = ['name']


class EditTeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ['name']


class NewMatchForm(forms.ModelForm):
    class Meta:
        model = Match
        fields = ['name', 'team_a', 'team_b', 'start', 'end', 'end_betting']
    
    def __init__(self, group, *args, **kwargs):
        super(NewMatchForm, self).__init__(*args, **kwargs)
        self.fields['team_a'] = forms.ModelChoiceField(
            queryset=Team.objects.filter(league__group=group) \
                .order_by('league__name', 'name'),
            empty_label=None
        )
        self.fields['team_b'] = forms.ModelChoiceField(
            queryset=Team.objects.filter(league__group=group) \
                .order_by('league__name', 'name'),
            empty_label=None
        )

    def clean(self):
        cleaned_data = super(NewMatchForm, self).clean()
        
        try:
            team_a = cleaned_data['team_a']
        except KeyError:
            team_a = None
        try:
            team_b = cleaned_data['team_b']
        except KeyError:
            team_b = None
        try:
            start = cleaned_data['start']
        except KeyError:
            start = None
        try:
            end = cleaned_data['end']
        except KeyError:
            end = None
        try:
            end_betting = cleaned_data['end_betting']
        except KeyError:
            end_betting = None
        
        if team_a is not None and team_b is not None and team_a == team_b:
            self.add_error('team_a', 'Drużyny są identyczne.')
            self.add_error('team_b', 'Drużyny są identyczne.')
        
        if start is not None and start < timezone.now() + timezone.timedelta(minutes=150):
            self.add_error(
                'start',
                'Mecz musi się rozpoczynać conajmniej za 150 minut.'
            )
        
        if end is not None and end <= start:
            self.add_error(
                'end',
                'Koniec meczu musi następować po jego rozpoczęciu.'
            )
        
        if end_betting is not None and end_betting > start:
            self.add_error(
                'end_betting',
                'Koniec możliwości typowania ' \
                    + 'musi nastąpić najpóźniej w czasie rozpoczęcia meczu.'
            )
        
        if end_betting is not None and end_betting < timezone.now() + timezone.timedelta(minutes=120):
            self.add_error(
                'end_betting',
                'Koniec możliwości typowania ' \
                    + 'może nastąpić najwcześniej za 120 minut.'
            )
        return cleaned_data


class SetMatchResultForm(forms.ModelForm):
    class Meta:
        model = Match
        fields = ['result_a', 'result_b']
    
    def __init__(self, *args, **kwargs):
        super(SetMatchResultForm, self).__init__(*args, **kwargs)
        self.fields['result_a'] = forms.IntegerField(
            min_value=0,
            max_value=99,
            label=self.instance.team_a.name
        )
        self.fields['result_b'] = forms.IntegerField(
            min_value=0,
            max_value=99,
            label=self.instance.team_b.name
        )


class NewBetForm(forms.ModelForm):
    class Meta:
        model = Bet
        fields = ['bet_team_a', 'bet_team_b']
    
    def __init__(self, match, *args, **kwargs):
        super(NewBetForm, self).__init__(*args, **kwargs)
        self.fields['bet_team_a'] = forms.IntegerField(
            min_value=0,
            max_value=99,
            label=match.team_a.name
        )
        self.fields['bet_team_b'] = forms.IntegerField(
            min_value=0,
            max_value=99,
            label=match.team_b.name
        )
