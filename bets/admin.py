from django.contrib import admin
from .models import Group, BetsTournament, League, Team, Match, Bet


class GroupAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'private', 'password', 'creator']


class BetsTournamentAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'group']


class LeagueAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'group']


class TeamAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'league']


class MatchAdmin(admin.ModelAdmin):
    list_display = ['id', 'bets_tournament', 'name', 'team_a', 'team_b', \
        'start', 'end', 'end_betting', 'result_a', 'result_b']


class BetAdmin(admin.ModelAdmin):
    list_display = ['id', 'match', 'user', 'bet_team_a', 'bet_team_b', \
        'timestamp', 'points']


admin.site.register(Group, GroupAdmin)
admin.site.register(BetsTournament, BetsTournamentAdmin)
admin.site.register(League, LeagueAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(Match, MatchAdmin)
admin.site.register(Bet, BetAdmin)
