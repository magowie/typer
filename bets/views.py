#!/usr/bin/python
# coding: utf-8
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import Group, BetsTournament, League, Team, Match, Bet, Points
from .forms import NewGroupForm, EditGroupForm, NewBetsTournamentForm, \
    EditBetsTournamentForm, NewLeagueForm, EditLeagueForm, NewTeamForm, \
    EditTeamForm, NewMatchForm, SetMatchResultForm, NewBetForm
from django.utils import timezone

@login_required
def groups(request):
    groups_list = Group.objects.all().order_by('name')
    return render(request, 'groups/groups.html', {'groups': groups_list})

@login_required
def new_group(request):
    if request.method == 'POST':
        new_group_form = NewGroupForm(request.POST)
        if new_group_form.is_valid():
            new_group = new_group_form.save(commit=False)
            new_group.creator = request.user
            new_group.save()
            new_group.admins.add(request.user)
            new_group.users.add(request.user)
            return redirect('groups')
    else:
        new_group_form = NewGroupForm()
    return render(
        request,
        'groups/new_group.html',
        {'new_group_form': new_group_form}
    )

@login_required
def join_to_group(request, group_id):
    try:
        group = Group.objects.get(id=group_id)
    except Group.DoesNotExist:
        group = None
    if group is not None and request.method == 'GET':
        if not request.user in group.users.all():
            if group.private:
                return redirect('join_to_priv_group', group_id=group_id)
            else:
                group.users.add(request.user)
        return redirect('group', group_id=group_id)
    return redirect('groups')

@login_required
def join_to_priv_group(request, group_id):
    try:
        group = Group.objects.get(id=group_id)
    except Group.DoesNotExist:
        group = None
    if group is not None and request.method == 'GET':
        if not request.user in group.users.all():
            password = request.GET.get('password')
            if password is None or password == '':
                return render(
                    request,
                    'groups/join_to_priv_group.html',
                    {
                        'group_name': group.name,
                        'group_id': group_id,
                        'password': None
                    }
                )
            if group.private and group.password != password:
                return redirect('join_to_priv_group', group_id=group_id)
            else:
                group.users.add(request.user)
        return redirect('group', group_id=group_id)
    return redirect('groups')

@login_required
def group(request, group_id):
    if request.method == 'GET':
        try:
            group = Group.objects.get(id=group_id)
        except Group.DoesNotExist:
            group = None
        if group is not None \
                and request.user in group.users.all():
            return render(request, 'groups/group.html', {'group': group})
    return redirect('groups')

@login_required
def edit_group(request, group_id):
    try:
        group = Group.objects.get(id=group_id)
    except Group.DoesNotExist:
        group = None
    if group is not None \
            and (request.user == group.creator
                or request.user in group.admins.all()):
        if request.method == 'POST':
            edit_group_form = EditGroupForm(instance=group, data=request.POST)
            if edit_group_form.is_valid():
                edit_group_form.save()
                messages.success(
                    request,
                    'Uaktualnienie parametrów grupy zakończyło się sukcesem.'
                )
                return render(
                    request,
                    'groups/group.html',
                    {'group_id': group_id}
                )
            else:
                messages.error(
                    request,
                    'Wystąpił błąd podczas zmiany parametrów grupy.'
                )
        else:
            edit_group_form = EditGroupForm(instance=group)
        return render(
            request,
            'groups/edit_group.html',
            {
                'edit_group_form': edit_group_form,
                'group_name': group.name
            }
        )
    return redirect('groups')

@login_required
def add_user_to_admins(request, group_id, user_id):
    if request.method == 'GET':
        try:
            group = Group.objects.get(id=group_id)
            user = User.objects.get(id=user_id)
        except Group.DoesNotExist:
            group = None
        except User.DoesNotExist:
            user = None
        if group is not None and user is not None \
                and user in group.users.all():
            if user not in group.admins.all() \
                    and (request.user in group.admins.all() \
                        or request.user == group.creator):
                group.admins.add(user)
            return redirect('group', group_id=group_id)
    return redirect('groups')

@login_required
def delete_user_from_admins(request, group_id, user_id):
    if request.method == 'GET':
        try:
            group = Group.objects.get(id=group_id)
            user = User.objects.get(id=user_id)
        except Group.DoesNotExist:
            group = None
        except User.DoesNotExist:
            user = None
        if group is not None and user is not None \
                and user in group.users.all():
            if user in group.admins.all() and request.user == group.creator:
                group.admins.remove(user)
            return redirect('group', group_id=group_id)
    return redirect('groups')

@login_required
def new_bets_tournament(request, group_id):
    try:
        group = Group.objects.get(id=group_id)
    except Group.DoesNotExist:
        group = None
    if group is not None and (request.user in group.admins.all() \
            or request.user == group.creator):
        if request.method == 'POST':
            new_bets_tournament_form = NewBetsTournamentForm(request.POST)
            bets_tournaments = group.bets_tournaments
            if not bets_tournaments \
                    .filter(name=new_bets_tournament_form['name'].value()) \
                    .exists():
                if new_bets_tournament_form.is_valid():
                    new_bets_tournament = new_bets_tournament_form \
                        .save(commit=False)
                    new_bets_tournament.group = group
                    new_bets_tournament.save()
                    new_bets_tournament.create_ranking()
                    return redirect('group', group_id=group_id)
            else:
                messages.error(
                    request,
                    'Turniej o podanej nazwie już istnieje w grupie.'
                )
        else:
            new_bets_tournament_form = NewBetsTournamentForm()
        return render(
            request,
            'tournaments/new_tournament.html',
            {'new_bets_tournament_form': new_bets_tournament_form}
        )
    return redirect('groups')

@login_required
def edit_bets_tournament(request, bets_tournament_id):
    try:
        bets_tournament = BetsTournament.objects.get(id=bets_tournament_id)
    except BetsTournament.DoesNotExist:
        bets_tournament = None
    if bets_tournament is not None \
            and (request.user in bets_tournament.group.admins.all() \
                or request.user == bets_tournament.group.creator):
        if request.method == 'POST':
            edit_bets_tournament_form = EditBetsTournamentForm(
                instance=bets_tournament,
                data=request.POST
            )
            bets_tournaments = bets_tournament.group.bets_tournaments
            if not bets_tournaments \
                    .filter(name=edit_bets_tournament_form['name'].value()) \
                    .exclude(id=bets_tournament_id).exists():
                if edit_bets_tournament_form.is_valid():
                    edit_bets_tournament_form.save()
                    messages.success(request, 'Zapisano.')
                    return render_bets_tournament(request, bets_tournament)
                else:
                    messages.error(
                        request,
                        'Wystąpił błąd podczas zmiany nazwy turnieju.'
                    )
            else:
                messages.error(
                    request,
                    'Turniej o podanej nazwie już istnieje w grupie.'
                )
        else:
            edit_bets_tournament_form = EditBetsTournamentForm(
                instance=bets_tournament
            )
        return render(
            request,
            'tournaments/edit_tournament.html',
            {
                'edit_bets_tournament_form': edit_bets_tournament_form,
                'tournament_name': bets_tournament.name
            }
        )
    return redirect('groups')

@login_required
def delete_bets_tournament(request, bets_tournament_id):
    if request.method == 'GET':
        try:
            bets_tournament = BetsTournament.objects.get(id=bets_tournament_id)
            group = bets_tournament.group
        except BetsTournament.DoesNotExist:
            bets_tournament = None
        if bets_tournament is not None:
            if request.user in group.admins.all() \
                    or request.user == group.creator:
                bets_tournament.delete()
            return redirect('group', group_id=group.id)
    return redirect('groups')

def render_bets_tournament(request, bets_tournament):
    now = timezone.now()
    matches_to_betting = bets_tournament.matches \
        .filter(end_betting__gt=now).order_by('end_betting')
    matches_to_wait = bets_tournament.matches \
        .filter(start__gt=now, end_betting__lte=now).order_by('-start')
    matches_being = bets_tournament.matches \
        .filter(start__lte=now, end__gt=now).order_by('end')
    matches_ended = bets_tournament.matches \
        .filter(end__lte=now, result_a__isnull=True).order_by('-start')
    matches_ended_with_result = bets_tournament.matches \
        .filter(end__lte=now, result_a__isnull=False).order_by('-start')
    group = bets_tournament.group
    
    return render(
        request,
        'tournaments/tournament.html',
        {
            'bets_tournament': bets_tournament,
            'matches_to_betting': matches_to_betting,
            'matches_to_wait': matches_to_wait,
            'matches_being': matches_being,
            'matches_ended' : matches_ended,
            'matches_ended_with_result': matches_ended_with_result,
            'group': group
        }
    )

@login_required
def bets_tournament(request, bets_tournament_id):
    if request.method == 'GET':
        try:
            bets_tournament = BetsTournament.objects.get(id=bets_tournament_id)
        except BetsTournament.DoesNotExist:
            bets_tournament = None
        if bets_tournament is not None \
                and request.user in bets_tournament.group.users.all():
            return render_bets_tournament(request, bets_tournament)
    return redirect('groups')

@login_required
def new_league(request, group_id):
    try:
        group = Group.objects.get(id=group_id)
    except Group.DoesNotExist:
        group = None
    if group is not None and (request.user in group.admins.all() \
            or request.user == group.creator):
        if request.method == 'POST':
            new_league_form = NewLeagueForm(request.POST)
            if not group.leagues \
                    .filter(name=new_league_form['name'].value()).exists():
                if new_league_form.is_valid():
                    new_league = new_league_form.save(commit=False)
                    new_league.group = group
                    new_league.save()
                    return redirect('leagues', group_id=group_id)
            else:
                messages.error(
                    request,
                    'Liga o podanej nazwie już istnieje w grupie.'
                )
        else:
            new_league_form = NewLeagueForm()
        return render(
            request,
            'leagues/new_league.html',
            {'new_league_form': new_league_form}
        )
    return redirect('groups')

@login_required
def edit_league(request, league_id):
    try:
        league = League.objects.get(id=league_id)
    except League.DoesNotExist:
        league = None
    if league is not None \
            and (request.user in league.group.admins.all() \
                or league.group.creator == request.user):
        if request.method == 'POST':
            edit_league_form = EditLeagueForm(
                instance=league,
                data=request.POST
            )
            if not league.group.leagues.filter(
                        name=edit_league_form['name'].value()
                    ).exclude(id=league_id).exists():
                if edit_league_form.is_valid():
                    edit_league_form.save()
                    messages.success(
                        request,
                        'Zapisano.'
                    )
                    return render(
                        request,
                        'leagues/league.html',
                        {'league': league}
                    )
                else:
                    messages.error(
                        request,
                        'Wystąpił błąd podczas zmiany nazwy ligi.'
                    )
            else:
                messages.error(
                    request,
                    'Liga o podanej nazwie już istnieje w grupie.'
                )
        else:
            edit_league_form = EditLeagueForm(
                instance=league
            )
        return render(
            request,
            'leagues/edit_league.html',
            {
                'edit_league_form': edit_league_form,
                'league_name': league.name
            }
        )
    return redirect('groups')

@login_required
def delete_league(request, league_id):
    if request.method == 'GET':
        try:
            league = League.objects.get(id=league_id)
            group = league.group
        except BetsTournament.DoesNotExist:
            league = None
        if league is not None:
            if request.user in group.admins.all() \
                    or request.user == group.creator:
                league.delete()
            return redirect('leagues', group_id=group.id)
    return redirect('groups')

def render_league(request, league):
    return render(
        request,
        'leagues/league.html',
        {'league': league, 'teams': league.teams.all().order_by('name')}
    )

@login_required
def league(request, league_id):
    if request.method == 'GET':
        try:
            league = League.objects.get(id=league_id)
        except League.DoesNotExist:
            league = None
        if league is not None \
                and request.user in league.group.users.all():
            return render_league(request, league)
    return redirect('groups')

@login_required
def leagues(request, group_id):
    try:
        group = Group.objects.get(id=group_id)
    except Group.DoesNotExist:
        group = None
    if group is not None and request.user in group.users.all():
        leagues_list = group.leagues.all().order_by('name')
        return render(
            request, 'leagues/leagues.html',
            {'leagues': leagues_list, 'group_id': group.id}
        )
    return redirect('groups')

@login_required
def new_team(request, league_id):
    try:
        league = League.objects.get(id=league_id)
    except League.DoesNotExist:
        league = None
    if league is not None \
            and (request.user in league.group.admins.all() \
            or request.user == league.group.creator):
        if request.method == 'POST':
            new_team_form = NewTeamForm(request.POST)
            if not Team.objects.filter(
                        name=new_team_form['name'].value(),
                        league__group=league.group
                    ).exists():
                if new_team_form.is_valid():
                    new_team = new_team_form.save(commit=False)
                    new_team.league = league
                    new_team.save()
                    return redirect('league', league_id=league_id)
            else:
                messages.error(
                    request,
                    'Drużyna o podanej nazwie już istnieje w grupie.'
                )
        else:
            new_team_form = NewTeamForm()
        return render(
            request,
            'teams/new_team.html',
            {'new_team_form': new_team_form}
        )
    return redirect('groups')

@login_required
def edit_team(request, team_id):
    try:
        team = Team.objects.get(id=team_id)
    except Team.DoesNotExist:
        team = None
    if team is not None \
            and (request.user in team.league.group.admins.all() \
                or request.user == team.league.group.creator):
        if request.method == 'POST':
            edit_team_form = EditTeamForm(
                instance=team,
                data=request.POST
            )
            if not Team.objects.filter(
                        name=edit_team_form['name'].value(),
                        league__group=team.league.group
                    ).exclude(id=team_id).exists():
                if edit_team_form.is_valid():
                    edit_team_form.save()
                    messages.success(
                        request,
                        'Zapisano.'
                    )
                    return render_league(request, team.league)
                else:
                    messages.error(
                        request,
                        'Wystąpił błąd podczas zmiany nazwy zespołu.'
                    )
            else:
                messages.error(
                    request,
                    'Drużyna o podanej nazwie już istnieje w grupie.'
                )
        else:
            edit_team_form = EditTeamForm(instance=team)
        return render(
            request,
            'teams/edit_team.html',
            {
                'edit_team_form': edit_team_form,
                'team_name': team.name
            }
        )
    return redirect('groups')

@login_required
def delete_team(request, team_id):
    if request.method == 'GET':
        try:
            team = Team.objects.get(id=team_id)
        except BetsTournament.DoesNotExist:
            team = None
        league = team.league
        group = league.group
        if league is not None:
            if request.user in group.admins.all() \
                    or request.user == group.creator:
                team.delete()
            return render_league(request, league)
    return redirect('groups')

@login_required
def teams(request, group_id):
    try:
        group = Group.objects.get(id=group_id)
    except Group.DoesNotExist:
        group = None
    if group is not None and request.user in group.users.all():
        teams_list = Team.objects.filter(league__group=group) \
            .order_by('league__name', 'name')
        return render(request, 'teams/teams.html', {'teams': teams_list})
    return redirect('groups')

@login_required
def new_match(request, bets_tournament_id):
    try:
        bets_tournament = BetsTournament.objects.get(id=bets_tournament_id)
    except BetsTournament.DoesNotExist:
        bets_tournament = None
    if bets_tournament is not None \
            and (request.user in bets_tournament.group.admins.all() \
                or request.user == bets_tournament.group.creator):
        if request.method == 'POST':
            new_match_form = NewMatchForm(bets_tournament.group, request.POST)
            matches = bets_tournament.matches
            if not matches.filter(name=new_match_form['name'].value()).exists():
                if new_match_form.is_valid():
                    new_match = new_match_form.save(commit=False)
                    new_match.bets_tournament = bets_tournament
                    new_match.save()
                    return redirect(
                        'bets_tournament',
                        bets_tournament_id=bets_tournament_id
                    )
            else:
                messages.error(
                    request,
                    'Mecz o podanej nazwie już istnieje w tym turnieju.'
                )
        else:
            new_match_form = NewMatchForm(bets_tournament.group)
        return render(
            request,
            'matches/new_match.html',
            {'new_match_form': new_match_form}
        )
    return redirect('groups')

@login_required
def set_match_result(request, match_id):
    try:
        match = Match.objects.get(id=match_id)
        group = match.bets_tournament.group
    except Match.DoesNotExist:
        match = None
    if match is not None \
            and (request.user in group.admins.all() \
                or request.user == group.creator):
        if match.end < timezone.now():
            if request.method == 'POST':
                set_match_result_form = SetMatchResultForm(
                    instance=match,
                    data=request.POST
                )
                if set_match_result_form.is_valid():
                    set_match_result_form.save()
                    match.bets_tournament.update_ranking(match_id)
                    messages.success(request, 'Wynik meczu został zapisany.')
                    return render_match(request, match)
                else:
                    messages.error(
                        request,
                        'Wystąpił błąd podczas zapisu wyniku meczu.'
                    )
            else:
                set_match_result_form = SetMatchResultForm(instance=match)
            return render(
                request,
                'matches/set_match_result.html',
                {
                    'set_match_result_form': set_match_result_form,
                    'match': match
                }
            )
        else:
            messages.error(
                request,
                'Nie ma możliwości zapisania wyniku ' \
                    + 'przed zakończeniem meczu.'
            )
            return render_bets_tournament(request, match.bets_tournament)
    return redirect('groups')

def render_match(request, match):
    if match.end_betting < timezone.now():
        bets = match.bets.exclude(user=request.user).order_by(
            'user__last_name', 'user__first_name', 'user__username'
        )
    else:
        bets = None
    try:
        user_bet = match.bets.get(user=request.user)
    except Bet.DoesNotExist:
        user_bet = None
    return render(
        request,
        'matches/match.html',
        {'match': match, 'bets': bets, 'user_bet': user_bet}
    )

@login_required
def match(request, match_id):
    try:
        match = Match.objects.get(id=match_id)
        group = match.bets_tournament.group
    except Match.DoesNotExist:
        match = None
    if match is not None and request.user in group.users.all():
        return render_match(request, match)
    return redirect('groups')

@login_required
def new_bet(request, match_id):
    try:
        match = Match.objects.get(id=match_id)
        group = match.bets_tournament.group
    except Match.DoesNotExist:
        match = None
    if match is not None and request.user in group.users.all():
        if not request.user.bets.filter(match=match).exists():
            now = timezone.now()
            if match.end_betting >= now:
                if request.method == 'POST':
                    new_bet_form = NewBetForm(match, request.POST)
                    new_bet = new_bet_form.save(commit=False)
                    new_bet.user = request.user
                    new_bet.match = match
                    new_bet.timestamp = now
                    new_bet.save()
                    return redirect('match', match_id=match_id)
                else:
                    new_bet_form = NewBetForm(match)
                return render(
                    request,
                    'bets/new_bet.html',
                    {'new_bet_form': new_bet_form}
                )
            else:
                messages.error(
                    request,
                    'Czas na typowanie tego meczu minął.'
                )
        else:
            messages.error(
                request,
                'Już obstawiłeś wynik tego meczu.'
            )
        return render_match(request, match)
    return redirect('groups')

@login_required
def users(request, bets_tournament_id):
    try:
        bets_tournament = BetsTournament.objects.get(id=bets_tournament_id)
        users = bets_tournament.group.users.all()
    except BetsTournament.DoesNotExist:
        bets_tournament = None
    if bets_tournament is not None and request.user in users:
        return render(request, 'bets/users.html', {'users': users})
    return redirect('groups')

@login_required
def bets(request, bets_tournament_id, user_id):
    try:
        bets_tournament = BetsTournament.objects.get(id=bets_tournament_id)
        user = User.objects.get(id=user_id)
        users = bets_tournament.group.users.all()
    except BetsTournament.DoesNotExist:
        bets_tournament = None
    except User.DoesNotExist:
        user = None
    if bets_tournament is not None and user is not None \
            and request.user in users and user in users:
        if user != request.user:
            bets = user.bets.filter(match__end_betting__lte=timezone.now())
        else:
            bets = user.bets.all()
        return render(
            request, 'bets/bets.html', {'bets': bets, 'user': user}
        )
    return redirect('groups')

@login_required
def rank(request, bets_tournament_id):
    try:
        bets_tournament = BetsTournament.objects.get(id=bets_tournament_id)
        users = bets_tournament.group.users.all()
    except BetsTournament.DoesNotExist:
        bets_tournament = None
    if bets_tournament is not None and request.user in users:
        if not bets_tournament.points.exists():
            bets_tournament.create_ranking()
        points = bets_tournament.points.order_by('position')
        return render(request, 'tournaments/ranking.html', {'points': points})
    return redirect('groups')
