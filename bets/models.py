#!/usr/bin/python
# coding: utf-8
from django.db import models
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator


class Group(models.Model):
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='created_groups'
    )
    admins = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='admin_of_groups'
    )
    users = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='member_of_groups'
    )
    name = models.CharField(blank=False, null=False, unique=True, max_length=40)
    private = models.BooleanField(default=False)
    password = models.CharField(
        blank=True,
        null=True,
        max_length=40
    )
    

def assign_points(bet_a, bet_b, result_a, result_b):
    if bet_a == result_a and bet_b == result_b:
        return 3
    elif bet_a > bet_b and result_a > result_b:
        return 1
    elif bet_a < bet_b and result_a < result_b:
        return 1
    elif bet_a == bet_b and result_a == result_b:
        return 1
    return 0


class BetsTournament(models.Model):
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        related_name='bets_tournaments'
    )
    name = models.CharField(
        blank=False,
        null=False,
        db_index=True,
        max_length=50
    )
    
    def __str__(self):
        return self.name
    
    def update_ranking_positions(self):
        sorted_points = self.points.order_by('-points')
        previous_position = 0
        draw_rows_number = 0
        previous_points = None
        for row in sorted_points:
            if previous_points is None or previous_points > row.points:
                row.position = previous_position + 1 + draw_rows_number
                previous_position = row.position
                draw_rows_number = 0
            else:
                row.position = previous_position
                draw_rows_number += 1
            previous_points = row.points
            row.save()
    
    def update_ranking(self, match_id):
        try:
            match = Match.objects.get(id=match_id)
        except Match.DoesNotExist:
            return False
        try:
            self.matches.get(id=match_id)
        except Match.DoesNotExist:
            return False
        if match.result_a is None or match.result_b is None:
            return False
        if self.points.exists() \
                and self.points.filter(user__isnull=False).count() \
                    == self.group.users.count():
            for bet in match.bets.all():
                user_points = self.points.get(user=bet.user)
                bet.points = assign_points(
                    bet.bet_team_a, bet.bet_team_b,
                    match.result_a, match.result_b
                )
                bet.save()
                user_points.points += bet.points
                user_points.save()
        else:
            self.create_ranking()
        return self.update_ranking_positions()
    
    def create_ranking(self):
        for user in self.group.users.all():
            try:
                self.points.get(user=user)
            except:
                Points.objects.create(bets_tournament=self, user=user)
        return True


class League(models.Model):
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        related_name='leagues'
    )
    name = models.CharField(
        blank=False, null=False, db_index=True, max_length=50
    )
    

class Team(models.Model):
    name = models.CharField(
        blank=False, null=False, db_index=True, max_length=50
    )
    league = models.ForeignKey(
        League,
        on_delete=models.CASCADE,
        related_name='teams'
    )
    
    def __str__(self):
        return self.name + ' (' + self.league.name + ')'


class Match(models.Model):
    bets_tournament = models.ForeignKey(
        BetsTournament,
        on_delete=models.CASCADE,
        related_name='matches'
    )
    name = models.CharField(
        blank=False,
        null=False,
        db_index=True,
        max_length=50
    )
    team_a = models.ForeignKey(
        Team,
        on_delete=models.SET_NULL,
        related_name='host_matches',
        null=True
    )
    team_b = models.ForeignKey(
        Team,
        on_delete=models.SET_NULL,
        related_name='guest_matches',
        null=True
    )
    start = models.DateTimeField(blank=False, null=False, db_index=True)
    end = models.DateTimeField(blank=False, null=False)
    end_betting = models.DateTimeField(blank=False, null=False, db_index=True)
    result_a = models.IntegerField(
        blank=True,
        null=True,
        default=None,
        validators=[MinValueValidator(0), MaxValueValidator(99)]
    )
    result_b = models.IntegerField(
        blank=True,
        null=True,
        default=None,
        validators=[MinValueValidator(0), MaxValueValidator(99)]
    )


class Bet(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='bets'
    )
    bet_team_a = models.IntegerField(
        blank=False,
        null=False,
        validators=[MinValueValidator(0), MaxValueValidator(99)]
    )
    bet_team_b = models.IntegerField(
        blank=False,
        null=False,
        validators=[MinValueValidator(0), MaxValueValidator(99)]
    )
    timestamp = models.DateTimeField(blank=False, null=False)
    match = models.ForeignKey(
        Match,
        on_delete=models.CASCADE,
        related_name='bets'
    )
    points = models.FloatField(blank=False, null=False, default=0)


class Points(models.Model):
    bets_tournament = models.ForeignKey(
        BetsTournament,
        on_delete=models.CASCADE,
        related_name='points'
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name='points',
        null=True
    )
    points = models.FloatField(blank=False, null=False, default=0)
    position = models.IntegerField(blank=True, null=False, default=1)
