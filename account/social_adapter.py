from allauth.account.adapter import DefaultSocialAccountAdapter
from django.contrib.auth.models import User


class SocialAdapter(DefaultSocialAccountAdapter):
    def pre_social_login(self, request, sociallogin):
        dfsad
        if sociallogin.is_existing:
            return
        if 'email' not in sociallogin.account.extra_data:
            return
        # check if given email address already exists.
        # Note: __iexact is used to ignore cases
        try:
            email = sociallogin.account.extra_data['email'].lower()
            user = User.objects.get(email__iexact=email)
        # if it does not, let allauth take care of this new social account
        except User.DoesNotExist:
            return
        # if it does, connect this new social login to the existing user
        sociallogin.connect(request, user)
