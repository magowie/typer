#!/usr/bin/python
# coding: utf-8
from django.shortcuts import render, redirect
from .forms import UserRegistrationForm, UserEditForm, ProfileEditForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.contrib.auth.models import User
from .models import Profile
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from .tokens import account_activation_token
from django.core.mail import EmailMessage


@login_required
def dashboard(request):
    return render(request, 'account/dashboard.html', {'section': 'dashboard'})

def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            # Utworzenie nowego obiektu użytkownika, ale jeszcze nie zapisujemy go w bazie danych.
            new_user = user_form.save(commit=False)
            new_user.is_active = False
            # Ustawienie wybranego hasła.
            new_user.set_password(
                user_form.cleaned_data['password']
            )
            # Zapisanie obiektu User.
            new_user.save()
            # Utworzenie profilu użytkownika.
            profile = Profile.objects.create(user=new_user)
            
            current_site = get_current_site(request)
            mail_subject = 'Typer - aktywacja konta'
            message = render_to_string(
                'acc_active_email.html', {
                'user': new_user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(new_user.pk)),
                'token': account_activation_token.make_token(new_user),
            })
            to_email = user_form.cleaned_data.get('email')
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            messages.success(
                request,
                'Link aktywujący konto został wysłany na twój adres e-mail.'
            )
            return render(request, 'home.html')
            # return HttpResponse('Link aktywujący konto został wysłany na twój adres e-mail.')
    else:
        user_form = UserRegistrationForm()
    return render(
        request,
        'account/register.html',
        {'user_form': user_form}
    )

@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(
            instance=request.user,
            data=request.POST
        )
        profile_form = ProfileEditForm(
            instance=request.user.profile,
            data=request.POST,
            files=request.FILES
        )
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(
                request,
                'Uaktualnienie profilu zakończyło się sukcesem.'
            )
        else:
            messages.error(
                request,
                'Wystąpił błąd podczas uaktualniania profilu.'
            )
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = None
        try:
            profile_form = ProfileEditForm(instance=request.user.profile)
        except Profile.DoesNotExist:
            profile = Profile.objects.create(user=request.user)
            profile_form = ProfileEditForm(instance=profile)
    return render(
        request,
        'account/edit.html',
        {'user_form': user_form,
        'profile_form': profile_form}
    )

def activate(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        # return redirect('home')
        # return HttpResponse('Konto zostało aktywowane, możesz się zalogować.')
        messages.success(
            request,
            'Konto zostało aktywowane, możesz się zalogować.'
        )
    else:
        # return HttpResponse('Link aktywacyjny jest niepoprawny!')
        messages.error(
            request,
            'Link aktywacyjny jest niepoprawny!'
        )
    return redirect('login')
