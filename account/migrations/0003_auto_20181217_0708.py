# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-12-17 07:08
from __future__ import unicode_literals

import account.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20181215_2150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='photo',
            field=models.ImageField(blank=True, upload_to=account.models.upload_to),
        ),
    ]
