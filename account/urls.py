#!/usr/bin/python
# coding: utf-8
from django.urls import re_path, path
from . import views
from django.contrib.auth import views as django_views

urlpatterns = [
    # Wzorce adresów URL dla widoków logowania i wylogowania.
    re_path(r'^login/$', django_views.LoginView.as_view(), name='login'),
    re_path(r'^logout/$', django_views.LogoutView.as_view(), name='logout'),
    re_path(
        r'^logout-then-login/$',
        django_views.logout_then_login,
        name='logout_then_login'
    ),
    re_path(r'^$', views.dashboard, name='dashboard'),

    # Adresy URL przeznaczone do obsługi zmiany hasła.
    re_path(
        r'^password-change/$',
        django_views.PasswordChangeView.as_view(),
        name='password_change'
    ),
    re_path(
        r'^password-change/done/$',
        django_views.PasswordChangeDoneView.as_view(),
        name='password_change_done'
    ),

    # Adresy URL przeznaczone do obsługi procedury zerowania hasła.
    re_path(
        r'^password-reset/$',
        django_views.PasswordResetView.as_view(),
        name='password_reset'
    ),
    re_path(
        r'^password-reset/done/$',
        django_views.PasswordResetDoneView.as_view(),
        name='password_reset_done'
    ),
    re_path(
        r'^password-reset/confirm/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/$',
        django_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'
    ),
    re_path(
        r'^password-reset/complete/$',
        django_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete'
    ),

    # Rejestracja konta użytkownika i jego profil.
    re_path(r'^register/$', views.register, name='register'),
    re_path(r'^edit/$', views.edit, name='edit'),

    path(
        'activate/<slug:uidb64>/<slug:token>/',
        views.activate,
        name='activate'
    ),
]
