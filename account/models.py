#!/usr/bin/python
# coding: utf-8
from django.db import models
from django.conf import settings

def upload_to(instance, filename):
    return 'users/{0}/{1}'.format(instance.user.username, filename)

class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    date_of_birth = models.DateField(blank=True, null=True)
    photo = models.ImageField(upload_to=upload_to, blank=True)

    def __str__(self):
        return 'Profil użytkownika {}.'.format(self.user.username)
